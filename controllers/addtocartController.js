const Cart = require("../models/addtocartModel");

// Display All CRUD Data
const cart_index = async (request, response) => {
  try {
    const getCart = await Cart.find();
    if (!getCart.length)
      return response.status(201).send("No cart at the moment");
    return response.status(201).json({ status: "success", data: getCart });
  } catch (error) {
    console.log(error);
    return response.status(400).send(error);
  }
};

// Create New CRUD
const cart_create_post = async (request, response) => {
  let cart = new Cart(request.body);
  try {
    await cart.save();
    return response.status(201).json({ status: "success", data: cart });
  } catch (error) {
    console.log(error);
    return response.status(422).send("cart add failed");
  }
};

module.exports = {
  cart_index,
  cart_create_post,
};
