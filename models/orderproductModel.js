const mongoose = require("mongoose");

const orderproductSchema = new mongoose.Schema({
  totalAmount: {
    type: Number,
  },
  isCheckOut: {
    type: Boolean,
    default: false,
  },
  products: [
    {
      productId: {
        type: String,
      },
      quantity: {
        type: Number,
      },
    },
  ],
});

module.exports = mongoose.model("Orderproduct", orderproductSchema);
